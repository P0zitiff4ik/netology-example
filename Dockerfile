FROM centos:7
RUN yum -y install wget make gcc openssl-devel bzip2-devel libffi-devel zlib-devel xz-devel && \
    cd /usr/src/ && \
    wget https://www.python.org/ftp/python/3.7.11/Python-3.7.11.tgz && \
    tar xzf Python-3.7.11.tgz && \
    cd Python-3.7.11 && \
    ./configure --enable-optimizations && \
    make altinstall && \
    rm /usr/src/Python-3.7.11.tgz && \
    ln -sfn /usr/local/bin/python3.7 /usr/bin/python3 && \
    ln -sfn /usr/local/bin/pip3.7 /usr/bin/pip3 && \
    python3 -m pip install --upgrade pip
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
COPY app.py /python_api/app.py
CMD ["python3", "/python_api/app.py"]
